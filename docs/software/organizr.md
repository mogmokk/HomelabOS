# Organizr

[Organizr](https://lamarios.github.io/organizr2/) is a simple dashboard that allows to monitor and interact with many different services.

## Access

It is available at [https://organizr.{{ domain }}/](https://organizr.{{ domain }}/) or [http://organizr.{{ domain }}/](http://organizr.{{ domain }}/)

{% if enable_tor %}
It is also available via Tor at [http://organizr.{{ tor_domain }}/](http://organizr.{{ tor_domain }}/)
{% endif %}
